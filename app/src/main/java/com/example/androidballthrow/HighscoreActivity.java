package com.example.androidballthrow;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.sql.Timestamp;

public class HighscoreActivity extends AppCompatActivity {

    private class highscoreEntry {
        private double score;
        private Timestamp timestamp;

        private highscoreEntry(double score, Timestamp timestamp) {
            this.score = score;
            this.timestamp = timestamp;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);
    }
}
