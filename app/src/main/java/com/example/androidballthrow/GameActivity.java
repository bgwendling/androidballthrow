package com.example.androidballthrow;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Locale;

public class GameActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager sensorManager;
    private Sensor linearAcceleration;

    private boolean launched = false;

    private TextView txt_throw_ball, txt_current_height, txt_max_height, lbl_current_height, lbl_max_height;

    private double highestVel;
    private static final double EARTHGRAV = 9.81;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        linearAcceleration = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        txt_throw_ball = findViewById(R.id.txt_throw_ball);
        txt_current_height = findViewById(R.id.txt_current_height);
        txt_max_height = findViewById(R.id.txt_max_height);

        lbl_current_height = findViewById(R.id.lbl_current_height);
        lbl_max_height = findViewById(R.id.lbl_max_height);

        txt_current_height.setVisibility(View.INVISIBLE);
        txt_max_height.setVisibility(View.INVISIBLE);

        lbl_current_height.setVisibility(View.INVISIBLE);
        lbl_max_height.setVisibility(View.INVISIBLE);

        highestVel = -1;
    }

    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, linearAcceleration, SensorManager.SENSOR_DELAY_NORMAL);
        highestVel = -1;
        launched = false;

        txt_throw_ball.setText(R.string.txt_throw_ball);

        txt_current_height.setVisibility(View.INVISIBLE);
        txt_max_height.setVisibility(View.INVISIBLE);

        lbl_current_height.setVisibility(View.INVISIBLE);
        lbl_max_height.setVisibility(View.INVISIBLE);
    }

    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        double xacc, yacc, zacc;
        xacc = event.values[0];
        yacc = event.values[1];
        zacc = event.values[2];

        //if using accelerometer purely earth gravity would have to be subtracted.
        double vel = Math.sqrt((xacc*xacc)+(yacc*yacc)+(zacc*zacc));
        if(vel > SettingsActivity.settings.forceThreshold) {
            if (vel > highestVel) {
                highestVel = vel;
            }
        }
        else{
            //if ball has been thrown and force is less than threshold:
            if(highestVel > 0 && !launched){
                AsyncBallThrow ballThrow = new AsyncBallThrow();
                ballThrow.execute(highestVel);
                launched = true;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    class AsyncBallThrow extends AsyncTask<Double, Double, Double> {

        boolean travelling = true;
        long timeSinceStart = System.currentTimeMillis();
        long oldTimeSinceStart = timeSinceStart;
        long deltaTime;
        double travelTime = 0;
        double currentHeight;
        double maxHeight = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            txt_throw_ball.setText(R.string.txt_ball_thrown);

            txt_current_height.setVisibility(View.VISIBLE);
            txt_max_height.setVisibility(View.VISIBLE);

            lbl_current_height.setVisibility(View.VISIBLE);
            lbl_max_height.setVisibility(View.VISIBLE);
        }

        @Override
        protected Double doInBackground(Double... doubles) {
            double initialVel = doubles[0];

            timeSinceStart = System.currentTimeMillis();
            oldTimeSinceStart = timeSinceStart;

            while(travelling){
                timeSinceStart = System.currentTimeMillis();
                deltaTime = (timeSinceStart-oldTimeSinceStart);

                travelTime += deltaTime/1000f;
                //math here
                //d = V0t + 0.5(at^2)
                currentHeight = initialVel*travelTime + (-EARTHGRAV*Math.pow(travelTime, 2))/2;
                if(currentHeight > maxHeight) {
                    maxHeight = currentHeight;
                }

                if(currentHeight <= 0) {
                    travelling = false;
                    currentHeight = 0;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lbl_max_height.setText(String.format(Locale.getDefault(), "%f", maxHeight));
                        lbl_current_height.setText(String.format(Locale.getDefault(), "%f", currentHeight));
                    }
                });

                oldTimeSinceStart = timeSinceStart;
            }

            maxHeight = Math.pow(initialVel,2)/(2*EARTHGRAV);
            return maxHeight;
        }

        @Override
        protected void onPostExecute(Double aDouble) {
            super.onPostExecute(aDouble);

            //get absolute max height.
            lbl_max_height.setText(String.format(Locale.getDefault(), "%f", aDouble));
        }
    }
}
